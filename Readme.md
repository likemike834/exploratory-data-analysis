# The Exploratory Data Analyzis of dummy dataset

You can find Jupyter notebook file [here](EDA.ipynb) or [here](https://colab.research.google.com/drive/1jM62wKaMED-L5PJQSaVDAL1ulNAtWIuk?usp=sharing)

## First look

![Data distribution](img/Histograms.png "Histograms")
![Data description](img/Statistics.png "Distribution parameters")

## Variables overview  

#### Column "Gender":  
       - Variable data type is String
       - Data has Bernouly distribution. Men outnumber Women by 8.2% 

#### Column "last_connection":

       - The variable data type is Datetime 
       - The variable has 100 missed values. 
       -  The month variable is unbalanced - 87 last connections in January vs 3068 in March. So we should analyze users whose last connection was in January separately from others or replace the date of last connection with a mode.
       - Data follows uniform distribution. The number of users who had the last connection in the first month is equivalent to the number of potentially lost users/employees, looks like it is an service or website due to uniformly distributed activity by days of week, possibly it is an 24/7 service. At the same time, the number of users in the last month of the period shows the actual number of users. The data represents first half of 2015 year. 

#### Column "x1":
       - The variable data type is float64 
       - Data follows uniform distribution with mean=50.057630, std=28.927412. Data range is aproximatly 1-100, probably units are percents. 

#### Column "x2":

       - The variable data type is float64 
       - There are 30 outliers in the column 
       - Data follows Studen's distribution with mean=300.066508 and std=9.882410 

#### Column "x3":

       - The variable data type is float64
       - There are 2 of outliers in the column 
       - Data follows Log-normal distribution with mean=95.064154, std=9.964247

#### Column "t1":

       - The variable data type is float64
       - Data follows Normal distribution with mean=1174.761003, std=118.643741

#### Column "t2":

       - The variable data type is float64
       - Data follows Bimodal distribution with mean=1769.220676 and mode1~1750 mode2~1865

## Explore Correlations

From scatter matrix are obvious positive correlations between t1->x1 and t2->x2  

![Scatter matrix](img/Scatter_matrix_full.png "Scatter matrix full")  

Correlation matrix:  

![Correlation matrix](img/Correlation_matrix.png "Correlation matrix")  

We can use variable x1 for variable t1 prediction and x2 for variable t2.  
Variables x1 and t1 have a **strong** correlation between each other.  
Variables x2 and t2 have a **moderated** correlation between each other.  

### Without math explanation:

![Selected features](img/features.png "Selected features")

## Potential for further analysis:

I recommend further investigation of the t2 variable distribution, I would like to separate the  
distribution into two parts and explore them separately. Also, I think that performing a distribution  
test on variable x3 would good idea.  
Also, we can find global outliers using PCA and then perform DBSCAN on it.  
